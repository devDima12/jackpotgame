public class SimpleRegistrationService {
    public User register(String[] args) {
        User user = User.init(args);

        System.out.println("Welcome " + user.name + "to the Jackpot Game");
        return user;
    }
}
