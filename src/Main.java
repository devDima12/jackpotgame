import java.util.Random;

public class Main {
    public static void main(String[] args) {

        User user = User.init(args);
        SimpleRegistrationService registrationService = new SimpleRegistrationService();

        SimpleGameService simpleGameService = new SimpleGameService();
        simpleGameService.play(user);

    }
}
