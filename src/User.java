public class User {
    String name;
    
    public User(String name){
        this.name = name;
    }

    public static User init(String[] args) {
        return new User(args[0]); 
    }
}
